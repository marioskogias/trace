struct zipkin_trace {
    char service_name[20];
    int port;
    char trace_id[36];
    char span_id[36];
    char parent_span_id[36];
    int kind; // 0 for timestamp 1 for key-val
    int initial;
    char key[20];
    char val[50];
    long long int timestamp;
};
