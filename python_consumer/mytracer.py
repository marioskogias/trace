from scribe import scribe
from thrift.transport import TTransport, TSocket
from thrift.protocol import TBinaryProtocol
from collections import defaultdict
from formatters import base64_thrift_formatter

class Tracer():
    #DEFAULT_END_ANNOTATIONS = ("ss", "cr", "blocker_receive")
    DEFAULT_END_ANNOTATIONS = ("ss", "cr")

    def __init__(self, host, port):
        self._annotations_for_trace = defaultdict(list)
        socket = TSocket.TSocket(host=host, port=port)
        transport = TTransport.TFramedTransport(socket)
        protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
        self.client = scribe.Client(protocol)
        transport.open()

    def scribe_log(self, trace, annotations):
        trace._endpoint = None
        message = base64_thrift_formatter(trace, annotations)
        category = 'zipkin'
        log_entry = scribe.LogEntry(category, message)
        result = self.client.Log(messages=[log_entry])

    def record(self, trace, annotation):
        #self.scribe_log(trace, [annotation])
        #print annotation.endpoint.service_name, annotation.name, annotation.value
        trace_key = (trace.trace_id, trace.span_id)
        self._annotations_for_trace[trace_key].append(annotation)
        if (annotation.name in self.DEFAULT_END_ANNOTATIONS):
            saved_annotations = self._annotations_for_trace[trace_key]
            del self._annotations_for_trace[trace_key]
            self.scribe_log(trace, saved_annotations)
