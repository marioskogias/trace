from ctypes import *



class zipkin_trace(Structure):
    pass
zipkin_trace._fields_ = [
    ('service_name', c_char * 20),
    ('port', c_int),
    ('trace_id', c_char * 36),
    ('span_id', c_char * 36),
    ('parent_span_id', c_char * 36),
    ('kind', c_int),
    ('initial', c_int),
    ('key', c_char * 20),
    ('val', c_char * 50),
    ('timestamp', c_longlong),
]
__all__ = ['zipkin_trace']
