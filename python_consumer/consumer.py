#!/usr/bin/python
import os
import sys
import time
import mmap
import posix_ipc
from trace import Annotation, Trace, Endpoint
from mytracer import Tracer

from trace_type import zipkin_trace
from ctypes import *
SIZE = 10000

def send_trace(log_trace, tracer):
    service = log_trace.service_name
    port = log_trace.port
    trace_id = log_trace.trace_id
    trace_id = int(trace_id.replace("-", "")[:12], 16)
    span_id = log_trace.span_id
    span_id = int(span_id.replace("-", "")[:12], 16)
    if log_trace.initial:
        parent_span = None
    else:
        parent_span = log_trace.parent_span_id
        parent_span = int(parent_span.replace("-", "")[:12], 16)

    #create endpoint
    trace = Trace(service, trace_id, span_id, parent_span)
    endpoint = Endpoint("0.0.0.0",int(port), service)
    trace.set_endpoint(endpoint)
    if trace_id:
        if log_trace.kind:  # 0 for timestamp 1 for key-value
            key = log_trace.key
            val = log_trace.val
            a =  Annotation.string(key, val)
            a.endpoint = endpoint
            tracer.record(trace, a)
        else:
            timestamp = log_trace.timestamp
            event = log_trace.val
            a =  Annotation.timestamp(event, int(timestamp))
            a.endpoint = endpoint
            tracer.record(trace, a)
            #print a.endpoint.service_name

class FileDes():
    def __init__(self, f1, f2, name):
        f1.seek(sizeof(c_int))
        self.fd=f1
        self.tail_fd = f2
        self.head = 0;
        self.name = name

    def get_count(self):
        temp = self.tail_fd.read(sizeof(c_int))
        buf = create_string_buffer(temp)
        tail = c_int.from_buffer_copy(buf).value
        self.tail_fd.seek(0)
        tail = max(0, tail-2)
        #print self.head, tail
        if (self.head > tail):
            count = SIZE - self.head
            self.head = 0
        else:
            count = tail - self.head
            self.head = tail
        return count

    def get_trace(self):
        temp = self.fd.read(sizeof(zipkin_trace))
        buf = create_string_buffer(temp)
        if self.head == 0 :
            self.fd.seek(sizeof(c_int))
        return zipkin_trace.from_buffer_copy(buf)

    def parse_log_and_send(self, count, tracer):
        for i in range(count):
            t = self.get_trace()
            send_trace(t, tracer)



def main():
    files_list = []
    for f in sys.argv[1:]:
        mem = posix_ipc.SharedMemory(f)
        mapfile1 = mmap.mmap(mem.fd, 0)
        mapfile2 = mmap.mmap(mem.fd, 0)
        files_list.append(FileDes(mapfile1, mapfile2, f))

    t = Tracer("83.212.111.249", 9410)

    while(1):
        for f in files_list:
            count = f.get_count()
            if count>0:
                f.parse_log_and_send(count, t)

if __name__ == '__main__':
    main()
